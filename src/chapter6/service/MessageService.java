package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            /*
             * idをnullで初期化
             * ServletからuserIdの値が渡ってきていたら
             * 整数型に型変換し、idに代入
             */
            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            if(!StringUtils.isEmpty(start)) {
            	start = start + " 00:00:00";
            }else {
            	start = "2022-01-01 00:00:00";
            }

            if(!StringUtils.isEmpty(end)) {
            	end = end + " 23:59:59";
            }else {
            	Date date = new Date();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            	end = df.format(date);
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, start, end, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		new MessageDao().delete(connection, id);
    		commit(connection);
    	} catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch(Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public Message select(int id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		Message message = new MessageDao().select(connection, id);
    		commit(connection);

    		return message;
    	} catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch(Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void update(Message message) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		new MessageDao().update(connection, message);
    		commit(connection);
    	} catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch(Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}